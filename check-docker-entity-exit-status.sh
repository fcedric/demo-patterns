#!/usr/bin/env bash

DOCKER_ENTITY=
MODE="prod"

# get parameters
for i in "$@"
do
case ${i} in
    -h|--help)
        printf "\n=================================================="
        printf "\n: Check container exit status :"
        printf "\n==================================================\n"
        printf "\nCheck exit status code of a docker compose service or a docker container."
        printf "\nAnd exit this script with the same exit code."
        printf "\n\n"
        printf "\nUsage : ./check-docker-entity-exit-status.sh -de=<docker-entity> [-m=(test|prod)]"
        printf "\n"
        printf "\n  -m,--mode : define the type of container whose you want to check the exit status."
        printf "\n              Possible values : 'prod' | 'test'"
        printf "\n              Default value : 'prod'"
        printf "\n"
        printf "\n  -de,--docker-entity : the Docker compose service name or the container name."
        printf "\n\n"
    exit 0
    ;;
    -m=*|--mode=*)
    MODE="${i#*=}"
    shift # past argument=value
    ;;
    -de=*|--docker-entity=*)
    DOCKER_ENTITY="${i#*=}"
    shift # past argument=value
    ;;
esac
done

# check parameters
if [ ! "${DOCKER_ENTITY}" ]; then
  printf "\n\nYou are not set the docker entity name. Please specify it.";
  printf "\nCall -h or --help for more informations.\n";
  exit 1;
fi;

  # Get exit status code
printf "Check exit status of docker entity '${DOCKER_ENTITY}' in '${MODE}' mode.\n\n"
if [ "${MODE}" == "prod" ]
then
  exitStatus=$(docker-compose ps -q "${DOCKER_ENTITY}" | xargs docker inspect -f '{{ .State.ExitCode }}')
elif [ "${MODE}" == "test" ]
then
  exitStatus=$(docker-compose -f docker-compose.yml -f docker-compose.test.yml ps -q "${DOCKER_ENTITY}" | xargs docker inspect -f '{{ .State.ExitCode }}')
else
  printf "\n\nYou have entered an unknown mode.";
  printf "\nCall -h or --help for more informations.\n";
  exit 1;
fi

exit "${exitStatus}"
