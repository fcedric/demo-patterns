# Project structure

```shell script
# Create workspace with default application and routing
ng new DemoPatterns --directory=demo-patterns --prefix=dp --style=scss --routing --skipGit

# install additionals dependencies
npm i -D karma-mocha-reporter
npm i -D @compodoc/compodoc
ng add @ngrx/store @ngrx/effect 
# npn install / ng add ...

# Code Format
npm i -D husky lint-staged prettier
./node_modules/.bin/prettier --write "src/**/*.{js,jsx,ts,tsx,json,css,scss,md}"```
```

# Application structure

```shell script
## generate core
# models
ng g interface core/models/user --type=model
ng g interface core/models/address --type=model
# services
ng g service core/api/user
# form validators
ng g module core/form/validators
ng g directive core/form/validators/UserRoleValidator

## generate shareds
ng g module shared/user
ng g component shared/user/userForm

## generate pages
# home
ng g module pages/home --routing
ng g @angular/material:dashboard pages/home/home
# mvc
ng g module pages/mvc --routing
ng g component pages/mvc/user
```
