FROM nginx:1.17.2-alpine

ARG NGINX_CONFIG_FILE_PATH
ARG SOURCES_PATH

## Copy our default nginx config
COPY $NGINX_CONFIG_FILE_PATH /etc/nginx/nginx.conf
COPY nginx/conf/ /etc/nginx/conf/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY $SOURCE_PATH /usr/share/nginx/html/

CMD ["nginx", "-g", "daemon off;"]
