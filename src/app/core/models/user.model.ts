import { Adress, addressFactory } from './address.model';

// Models definitions

export enum UserRoles {
  Admin = 'admin',
  Undefined = 'undefined'
}

export interface UserBase {
  name: string;
  age: number;
  role: UserRoles;
  address?: Adress;
}

export interface UserAdmin extends UserBase {
  role: UserRoles.Admin;
  foo?: string;
}

export interface UserUndefined extends UserBase {
  role: UserRoles.Undefined;
  bar?: string;
}

export type User = UserAdmin | UserUndefined;
export type UserKeys = keyof UserAdmin | keyof UserUndefined;

// Type Guards

function isUser(obj: any): obj is UserBase {
  return (
    typeof obj === 'object' &&
    obj.hasOwnProperty('name') &&
    typeof obj.name === 'string' &&
    obj.hasOwnProperty('age') &&
    typeof obj.age === 'number' &&
    obj.hasOwnProperty('role') &&
    typeof obj.role === 'string'
  );
}

export function isUserAdmin(obj: any): obj is UserAdmin {
  return isUser(obj) && obj.role === UserRoles.Admin;
}

export function isUserUndefined(obj: any): obj is UserUndefined {
  return isUser(obj) && obj.role === UserRoles.Undefined;
}

// Model Factory

export function userFactory(obj: string | Partial<User> = {}, deepCopy: boolean = false): User {
  const defaultUser: User = {
    name: '',
    age: 0,
    role: UserRoles.Undefined
  };

  const parsedUser = typeof obj === 'string' ? (JSON.parse(obj) as Partial<User>) : obj;

  if (deepCopy && parsedUser.address) {
    parsedUser.address = addressFactory(parsedUser.address);
  }
  return {
    ...defaultUser,
    ...parsedUser
  };
}

// Examples

/**
 * Exemple d'utilisation avec des Type Guards
 */
function getUserSpecialAttributWithTypeGuards(user: User) {
  // is UserAdmin | UserUndefined (UserBase)
  if (isUserAdmin(user)) {
    // is UserAdmin
    return user.foo;
  } else if (isUserUndefined(user)) {
    // is UserUndefined
    return user.bar;
  } else {
    // is never
    return null;
  }
}

/**
 * Exemple d'utilisation avec des Types
 */
function getUserSpecialAttributWithTypes(user: User) {
  // is UserAdmin | UserUndefined (UserBase)
  switch (user.role) {
    case UserRoles.Admin:
      // is UserAdmin
      return user.foo;
    case UserRoles.Undefined:
      // is UserUndefined
      return user.bar;
    default:
      // is never
      return null;
  }
}
