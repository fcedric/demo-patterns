export interface Adress {
  street: string;
}

export function isAdress(obj: any): obj is Adress {
  return typeof obj === 'object' && obj.hasOwnProperty('street') && typeof obj.street === 'string';
}

export function addressFactory(obj: string | Partial<Adress> = {}) {
  const defaultAdress: Adress = { street: '' };
  const parsedAdress = typeof obj === 'string' ? (JSON.parse(obj) as Adress) : obj;
  return { ...defaultAdress, ...parsedAdress };
}
