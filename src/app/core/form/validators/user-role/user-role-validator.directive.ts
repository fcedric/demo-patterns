import { Directive } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { UserRoles } from '../../../models/user.model';

export interface UserRoleValidatorError extends ValidationErrors {
  readonly userRole: { text: string };
}

export const userRoleValidator: ValidatorFn = (control: FormControl): UserRoleValidatorError | null => {
  const role = control.value;
  return role === UserRoles.Admin || role === UserRoles.Undefined
    ? null
    : { userRole: { text: `le role de l'utilisateur n'est pas correcte` } };
};

@Directive({
  selector: '[dpUserRoleValidator]'
})
export class UserRoleValidatorDirective implements Validator {
  constructor() {}

  validate(control: AbstractControl): ValidationErrors | null {
    return userRoleValidator(control);
  }
}
