import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoleValidatorDirective } from './user-role/user-role-validator.directive';

@NgModule({
  declarations: [UserRoleValidatorDirective],
  imports: [CommonModule],
  exports: [UserRoleValidatorDirective]
})
export class ValidatorsModule {}
