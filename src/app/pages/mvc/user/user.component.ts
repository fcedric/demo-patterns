import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/api/user.service';
import { User, UserRoles } from '../../../core/models/user.model';

@Component({
  selector: 'dp-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: User;
  submittedUser: User;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getUsers().subscribe(users => (this.user = users[0] || null));
  }

  isAdminText(user: User) {
    return user?.role === UserRoles.Admin ? '(is admin)' : '';
  }

  onSubmit(user: User) {
    this.submittedUser = user;
  }
}
