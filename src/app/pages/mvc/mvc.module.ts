import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserModule } from '../../shared/user/user.module';
import { MvcRoutingModule } from './mvc-routing.module';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [UserComponent],
  imports: [CommonModule, MvcRoutingModule, UserModule]
})
export class MvcModule {}
