import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'dp-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'MVC', route: ['/mvc'], cols: 3, rows: 1 },
          { title: 'FLUX', route: ['/flux'], cols: 3, rows: 1 },
          { title: 'REDUX', route: ['/redux'], cols: 3, rows: 1 }
        ];
      }

      return [
        { title: 'MVC', route: ['/mvc'], cols: 1, rows: 1 },
        { title: 'FLUX', route: ['/flux'], cols: 1, rows: 1 },
        { title: 'REDUX', route: ['/redux'], cols: 1, rows: 1 }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
