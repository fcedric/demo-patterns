import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { userRoleValidator } from '../../../core/form/validators/user-role/user-role-validator.directive';
import { User, UserAdmin, UserBase, userFactory, UserKeys, UserRoles, UserUndefined } from '../../../core/models/user.model';

@Component({
  selector: 'dp-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Input() user?: User;
  @Output() submitUser = new EventEmitter<User>();

  userForm = this.fb.group({
    name: [null, Validators.required],
    age: [18, [Validators.required, Validators.min(18)]],
    role: [UserRoles.Undefined, [Validators.required, userRoleValidator]],
    address: this.fb.group({
      street: [null]
    })
  });

  get userNameControl() {
    return this.userForm.controls?.name as FormControl;
  }

  get userAgeControl() {
    return this.userForm.controls?.age as FormControl;
  }

  get userRoleControl() {
    return this.userForm.controls?.role as FormControl;
  }

  get userFooBarControl(): { control: FormControl; name: string } {
    switch (this.userRoleControl.value) {
      case UserRoles.Admin:
        return {
          name: 'foo',
          control: this.userForm.controls?.foo as FormControl
        };
      case UserRoles.Undefined:
        return {
          name: 'bar',
          control: this.userForm.controls?.bar as FormControl
        };
    }
    return null;
  }

  roles: typeof UserRoles = UserRoles;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.userRoleControl.valueChanges.subscribe(value => this.onUserRoleChange(value));
    this.userForm.patchValue(userFactory(this.user, true));
  }

  onSubmit(): void {
    if (this.userForm.valid) {
      this.submitUser.emit(this.userForm.value);
    }
  }

  private onUserRoleChange(value: UserRoles | null): void {
    switch (value) {
      case UserRoles.Admin:
        this.addControl<UserAdmin>('foo');
        this.removeControl('bar');
        break;
      case UserRoles.Undefined:
        this.addControl<UserUndefined>('bar');
        this.removeControl('foo');
        break;
      default:
        this.removeControl('bar');
        this.removeControl('foo');
    }
  }

  private addControl<T extends UserBase = UserBase>(
    name: keyof T,
    value = '',
    validadors: ValidatorFn | ValidatorFn[] | AbstractControlOptions = null
  ): void {
    if (typeof name === 'string' && !this.userForm.contains(name)) {
      this.userForm.addControl(name, this.fb.control(value, validadors));
    }
  }

  private removeControl(name: UserKeys): void {
    if (typeof name === 'string' && this.userForm.contains(name)) {
      this.userForm.removeControl(name);
    }
  }
}
